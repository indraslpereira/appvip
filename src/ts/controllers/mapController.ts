///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module AppControllers {
    'use strict';
    class MapController {
        data: string[];
        map;
        options;
        //static $inject = ['$http'];
        constructor() {
            //Controller Body
            this.data = ['valor 1', 'valor 2'];
            this.map = {center: {latitude:45, longitude: -73}, zoom: 14};
        }
    }
    angular.module('app.controllers')
        .controller('app.controllers.MapController', MapController);
}

//For use inside routes definition app.ts:
//.state('test', {
//                        url: '/',
//                        templateUrl: 'templates/-template.html',
//                        controller: 'EjemploApp.controllers.MapController as '
//                    });

//For use inside template:
//    {{.data}}

//Check dependencies inside app.ts
//    angular.module('EjemploApp.controllers', []);
//    angular.module('app', ['ionic', 'EjemploApp.controllers'])

//Check insertion of javascript file inside index.html
//<script src="js/app.js" type="application/javascript"></script>
//<script src="js/controllers/controller.js"></script>
