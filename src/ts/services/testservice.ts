/**
 * Created by ralopezn on 26/03/2015.
 */
///<reference path="../../../tools/typings/tsd.d.ts" />
///<reference path="../../../tools/typings/typescriptApp.d.ts" />

module appServices {

    export interface ITestFactory {
        getData: {(): string[]};
    }

    TestFactory.$inject = ['$scope'];
    function TestFactory($scope) : ITestFactory {

        var data: string[];

        data = [
            'value 1',
            'value 2'
        ];

        return {
            getData: (): string[] => { return data; }
        };
    }

    angular.module('app.services')
        .factory('app.services.Test', TestFactory);

}