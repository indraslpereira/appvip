/**
 * Created by ralopezn on 31/03/2015.
 */
///<reference path="../../tools/typings/tsd.d.ts" />
///<reference path="../../tools/typings/typescriptApp.d.ts" />

((): void => {

    angular.module('app.controllers', []);
    angular.module('app.dependencies', [
        'app.controllers'
    ]);

})();